/**
* Clase para motor deportivo.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Deportivo implements Motor {

  /*Varibale para guardar el tipo de motor*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Deportivo(){
    tipo = "Motor Deportivo";
  }

  /**
  * Metodo para crear la motor.
  */
  public void creaMotor(){
    tipo = "Motor Deportivo";
  }

  /**
  * Metodo que regresa el tipo de motor.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del motor.
  * @return el costo del motor.
  */
  public double getCosto(){
    return 1700.39;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 5;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 8;
  }

}//clas
