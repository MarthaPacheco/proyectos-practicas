/**
* Interface para tipo de arma.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Arma {

  /**
  * Metodo para crear el arma.
  */
  public void creaArma();

  /**
  * Metodo que regresa el tipo de arma.
  */
  public void getTipo();

  /**
  * Metdo para el costo del arma.
  * @return costo del arma.
  */
  public double getCosto();

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa();

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque();

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad();

}//clas
