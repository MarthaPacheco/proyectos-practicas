/**
* Clase para los nombres.
* Este codigo no es mio y lo saque de la siguiente pagina:
* https://yoandroide.xyz/generar-nombres-aleatorios-en-java/
* @author Yo Andoide.
* @version 1.0.
*/

public class NombresAleatorios {

	/**
	* Este metodo genera nombres con apellidos. El proceso es aleatorio.
	* Cada vez que se corra el programa mostrara nombres diferentes.
	* @param cantidad cantidad de nombres que se quieren generar.
	* @return un arreglo de String con los nombres y apellidos generados. El
	* formato de salida es: Nombre Apellido
	*/
	public static String[] generarNombresAleatorios(int cantidad) {
		String[] nombresAleatorios = new String[cantidad];

		String[] nombres = { "Andrea", "David", "Baldomero", "Balduino", "Baldwin",
				"Baltasar", "Barry", "Bartolo", "Bartolomé", "Baruc", "Baruj",
				"Candelaria", "Cándida", "Canela", "Caridad", "Carina", "Carisa",
				"Caritina", "Carlota", "Baltazar"};
		String[] apellidos = { "Gomez", "Guerrero", "Cardenas", "Cardiel", "Cardona",
				"Cardoso", "Cariaga", "Carillo","Carion", "Castiyo", "Castorena",
				"Castro", "Grande", "Grangenal", "Grano", "Grasia", "Griego", "Grigalva" };

		for (int i = 0; i < cantidad; i++) {
			nombresAleatorios[i] = nombres[(int) (Math.floor(Math.random() * ((nombres.length - 1) - 0 + 1) + 0))] + " "
					+ apellidos[(int) (Math.floor(Math.random() * ((apellidos.length - 1) - 0 + 1) + 0))];
		}
		return nombresAleatorios;
	}

}
