/**
* Clase para el Peloton.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Peloton3K extends Peloton {

  /**
  * Constructor de la clase.
  */
  public Peloton3K(){
    String[] nombre = NombresAleatorios.generarNombresAleatorios(5);
    comandante = new Comandante(nombre[0], new Caballeria());
    peloton.add(comandante);
    peloton.add(new Soldado(nombre[1], new Caballeria()));
    peloton.add(new Soldado(nombre[2], new Caballeria()));
    peloton.add(new Soldado(nombre[3], new Caballeria()));
    peloton.add(new Soldado(nombre[4], new Caballeria()));
    comandante.setSoldados(peloton);
  }
}
