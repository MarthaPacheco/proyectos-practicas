/**
* Clase para la fabrica que produce las partes.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class FactoryProducer{

  /**
  * Metodo que regresa los componentes de un carro.
  * @param opcion tipo de componente a agregar al carro.
  * @return regresa la fabrica que se debe de crear.
  */
  public static AbstractFactory getFactory(String opcion){
    if(opcion == null){
      return null;
    }
    String car = opcion.toLowerCase();
    switch (car) {
      case "llanta":
        return new Llantas();
      case "carroceria":
        return new Carrocerias();
      case "motor":
        return new Motores();
      case "armas":
        return new Armas();
      case "blindajes":
        return new Blindajes();
      default:
        return null;
    }
	}
}//class
