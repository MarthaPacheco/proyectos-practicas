/**
* Clase para el tipo de pan Integral.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Integral extends Baguette {

  /*Atributo para guardar */


  /**
  * Constructor de clase.
  */
  public Integral(){
    tipoBa = "Integral";
  }

  /**
  * Metodo para el costo por el tipo de pan.
  * @return el costo del producto.
  */
  @Override
  public double costo(){
    return 1.50;
  }

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  @Override
  public double getTotal(){
    return costo();
  }

  /**
  * Metodo para imprimir el costo.
  */
  @Override
  public void imprimeCompra(){
    System.out.println("Pan Integral " + " --- " + costo());
  }

}//class
