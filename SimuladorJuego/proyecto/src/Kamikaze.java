/**
* Clase para el Ejercito.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Kamikaze extends BuildEjercito {

  /**
  * Constructor de la clase.
  */
  public Kamikaze(){
    peloton1 = new Peloton12K();
    peloton2 = new Peloton12K();
    peloton3 = new Peloton3K();
  }

}
