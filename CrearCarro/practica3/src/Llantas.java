/**
* Clase para la fabrica de llantas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Llantas extends AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  */
  @Override
	public Object getComponente(String tipoComponente){
		return getLLantas(tipoComponente);
	}

  /**
  * Metodo que regresa el tipo de llanatas.
  */
  public Llanta getLLantas(String tipoLlanta){
    if(tipoLlanta == null){
      return null;
    }
    String car = tipoLlanta.toLowerCase();
    switch (car) {
      case "deportivas":
        return new Deportivas();
      case "off-road":
        return new Off();
      case "simple":
        return new Simple();
      case "oruga":
        return new Oruga();
      default:
        return null;
    }
  }
}//class
