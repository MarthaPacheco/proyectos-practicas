/**
* Clase para la llanta oruga.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Oruga implements Llanta {

  /*Variable para guardar el tipo de llanta*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Oruga(){
    tipo = "Llanta Oruga de tanque";
  }

  /**
  * Metodo para crear la llanta.
  */
  public void creaLlanta(){
    tipo = "Llanta Oruga de tanque";
  }

  /**
  * Metodo que regresa el tipo de llanta.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo de la llanta.
  * @return el costo de la llanta.
  */
  public double getCosto(){
    return 600.00;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 10;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 1;
  }

}//clas
