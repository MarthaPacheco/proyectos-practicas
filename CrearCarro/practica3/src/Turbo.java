/**
* Clase para motor turbo.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Turbo implements Motor {

  /*Variable para guardar el tipo de motor*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Turbo(){
    tipo = "Motor Turbo";
  }

  /**
  * Metodo para crear la motor.
  */
  public void creaMotor(){
    tipo = "Motor Turbo";
  }

  /**
  * Metodo que regresa el tipo de motor.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del motor.
  * @return el costo del motor.
  */
  public double getCosto(){
    return 1845.65;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 1;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 3;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 10;
  }

}//clas
