import java.util.Scanner;

/**
* Clase para prubar la implementacion.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class WaySub {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
		int opcion;

    do{
			System.out.println("¿Qué desea ordenar?\n" +
				"1.- Baguette\n" +
				"2.- Pizza\n" +
				"0.- Salir\n");

				opcion = sc.nextInt();

				switch(opcion){
					case 1:
            int op;
            System.out.println("¿Que pan quieres?\n" +
            "1.- Veggie\n" +
    				"2.- Integral\n" +
    				"3.- Italiano\n");
            op = sc.nextInt();
            Baguette baguette = null;

            switch (op) { //switch interno
              case 1:
                baguette = new Veggie();
                break;

              case 2:
                baguette = new Integral();
                break;

              case 3:
                baguette = new Italiano();
                break;

              default:
                System.out.println("Elige una opcion valida.\n");
                break;

            }//switch interno
            int op1;
            do {
              System.out.println("¿Que quieres agregar?\n" +
              "1.- Pollo\n" +
              "2.- Mostaza\n" +
              "3.- Mayonesa\n" +
              "4.- Catsup\n" +
              "5.- Cebolla\n" +
              "6.- Peperoni\n" +
              "7.- Jitomate\n" +
              "8.- Lechuga\n" +
              "9.- Jamon\n" +
              "0.- Terminaar orden\n");
              op1 = sc.nextInt();
              switch (op1) {
                case 1:
                  baguette = new Pollo(baguette);
                  break;
                case 2:
                  baguette = new Mostaza(baguette);
                  break;
                case 3:
                  baguette = new Mayonesa(baguette);
                  break;
                case 4:
                  baguette = new Catsup(baguette);
                  break;
                case 5:
                  baguette = new Cebolla(baguette);
                  break;
                case 6:
                  baguette = new Peperoni(baguette);
                  break;
                case 7:
                  baguette = new Jitomate(baguette);
                  break;
                case 8:
                  baguette = new Lechuga(baguette);
                  break;
                case 9:
                  baguette = new Lechuga(baguette);
                  break;
                case 0:
                  baguette.imprimeCompra();
                  System.out.println("Ticket: " + String.format("%.2f",baguette.getTotal()));
                  System.out.println();
                  break;
                default:
                  System.out.println("Elige una opcion valida.\n");
                break;
              }
            } while (op1 != 0);

            break;//case1

					case 2:
            int op2;
            Baguette baguette1 = null;
            do {
              System.out.println("¿Que tipo de pizza quieres?\n" +
              "1.- Napolitana\n" +
      				"2.- Cachichen\n" +
              "3.- De Pollo\n" +
              "4.- California\n" +
              "5.- TNT\n");
              op2 = sc.nextInt();
              switch (op2) {
                case 1:
                  baguette1 = new Napolitana();
                  break;
                case 2:
                  baguette1 = new Cachichen();

                  break;
                case 3:
                  baguette1 = new DePollo();
                  break;
                case 4:
                  baguette1 = new California();
                  break;
                case 5:
                  baguette1 = new TNT();
                  break;
                default:
                  System.out.println("Elige una opcion valida.\n");
              }
            } while (!(op2 > 0 && op2 < 6));
            baguette1.imprimeCompra();
            break;

					case 0:
						break;

					default:
						System.out.println("Elige una opcion valida.\n");
						break;

				}

		}while(opcion != 0);


  }//main
}//class
