/**
* Clase para arma canon.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Canon implements Arma {

  /*Varibale para guardar el tipo de arma*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Canon(){
    tipo = "Cañon";
  }

  /**
  * Metodo para crear el arma.
  */
  public void creaArma(){
    tipo = "Cañon";
  }

  /**
  * Metodo que regresa el tipo de arma.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del arma.
  * @return el costo del arma.
  */
  public double getCosto(){
    return 1013.34;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 5;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 10;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 2;
  }

}//clas
