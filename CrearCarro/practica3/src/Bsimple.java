/**
* Clase para blindaje simple.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Bsimple implements Blindaje {

  /*Varibale para guardar el tipo de blindaje*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Bsimple(){
    tipo = "Blindaje simple";
  }

  /**
  * Metodo para crear la blindaje.
  */
  public void creaBlindaje(){
    tipo = "Blindaje simple";
  }

  /**
  * Metodo que regresa el tipo de blindaje.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del blindaje.
  * @return el costo del blindaje.
  */
  public double getCosto(){
    return 1213.65;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 4;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 4;
  }

}//clas
