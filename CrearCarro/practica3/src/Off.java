/**
* Clase para la llanta Off-road.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Off implements Llanta {

  /*Varibale para guardar el tipo de llanta*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Off(){
    tipo = "Llanta Off-road";
  }

  /**
  * Metodo para crear la llanta.
  */
  public void creaLlanta(){
    tipo = "Llanta Off-road";
  }

  /**
  * Metodo que regresa el tipo de llanta.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo de la llanta.
  * @return el costo de la llanta.
  */
  public double getCosto(){
    return 452.15;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 6;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 7;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 4;
  }


}//clas
