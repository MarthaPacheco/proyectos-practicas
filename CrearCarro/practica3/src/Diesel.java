/**
* Clase para motor diesel.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Diesel implements Motor {

  /*Variable para guardar el tipo de motor*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Diesel(){
    tipo = "Motor Diesel";
  }

  /**
  * Metodo para crear la motor.
  */
  public void creaMotor(){
    tipo = "Motor Diesel";
  }

  /**
  * Metodo que regresa el tipo de motor.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del motor.
  * @return el costo del motor.
  */
  public double getCosto(){
    return 700.35;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 8;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 8;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 4;
  }

}//clas
