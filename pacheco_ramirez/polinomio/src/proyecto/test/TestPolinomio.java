package proyecto.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import proyecto.Polinomio;
import proyecto.Monomio;
import proyecto.Literal;
import java.util.LinkedList;

/**
 * Clase para pruebas unitarias de la clase {@link Polinomio}.
 */
public class TestPolinomio {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /**
     * Prueba unitaria para {@link Polinomio#toString}.
     */
    @Test
    public void testToString(){
      LinkedList<Monomio> lm = new LinkedList<Monomio>();
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l1.add(new Literal(4, 'x'));
      lm.add(new Monomio(4, l1));
      Polinomio p1 = new Polinomio(lm);
      Assert.assertEquals(p1.toString(), "4x^4 " );
      System.out.println(p1 + "primer polinomio");

      LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      LinkedList<Literal> li = new LinkedList<Literal>();
      l2.add(new Literal(6, 'z'));
      lm1.add(new Monomio(13, l2));
      li.add(new Literal(3, 'x'));
      lm1.add(new Monomio(8, li));
      Polinomio p2 = new Polinomio(lm1);
      System.out.println(p2 + "segundo polinomio");
      Assert.assertEquals(p2.toString(), "13z^6 + 8x^3 ");

      LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      LinkedList<Literal> li2 = new LinkedList<Literal>();
      LinkedList<Literal> li3 = new LinkedList<Literal>();
      l3.add(new Literal(5, 'y'));
      lm2.add(new Monomio(9, l3));
      li2.add(new Literal(8, 'x'));
      lm2.add(new Monomio(3, li2));
      li3.add(new Literal(2, 'z'));
      lm2.add(new Monomio(6, li3));
      Polinomio p3 = new Polinomio(lm2);
      Assert.assertEquals(p3.toString(), "9y^5 + 3x^8 + 6z^2 " );
      System.out.println(p3 + "tercer polinomio");

      LinkedList<Monomio> lm3 = new LinkedList<Monomio>();
      Polinomio p4 = new Polinomio(lm3);
      Assert.assertEquals(p4.toString(), "0");
      System.out.println(p4 + " cuarto polinomio");
    }

    /**
     * Prueba unitaria para {@link Polinomio#simplifica}.
     */
    @Test
    public void testSimplifica(){
      LinkedList<Monomio> lm = new LinkedList<Monomio>();
      LinkedList<Literal> l = new LinkedList<Literal>();
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l.add(new Literal(2, 'x'));
      l.add(new Literal(1, 'y'));
      lm.add(new Monomio(3, l));
      l1.add(new Literal(1, 'z'));
      lm.add(new Monomio(3, l1));
      l2.add(new Literal(2, 'x'));
      l2.add(new Literal(1, 'y'));
      lm.add(new Monomio(1, l2));
      l3.add(new Literal(1, 'z'));
      lm.add(new Monomio(4, l3));
      Polinomio p = new Polinomio(lm);
      LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l4.add(new Literal(2, 'x'));
      l4.add(new Literal(1, 'y'));
      lm1.add(new Monomio(4, l4));
      l5.add(new Literal(1, 'z'));
      lm1.add(new Monomio(7, l5));
      Polinomio p1 = new Polinomio(lm1);
      Assert.assertEquals(p.simplifica(), p1);

      LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
      LinkedList<Literal> l6 = new LinkedList<Literal>();
      LinkedList<Literal> l7 = new LinkedList<Literal>();
      LinkedList<Literal> l8 = new LinkedList<Literal>();
      LinkedList<Literal> l9 = new LinkedList<Literal>();
      l6.add(new Literal(1, 'x'));
      l6.add(new Literal(1, 'y'));
      lm2.add(new Monomio(2, l6));
      l7.add(new Literal(1, 'x'));
      l7.add(new Literal(1, 'y'));
      lm2.add(new Monomio(2, l7));
      l8.add(new Literal(1, 'x'));
      l8.add(new Literal(1, 'y'));
      lm2.add(new Monomio(3, l8));
      l9.add(new Literal(1, 'x'));
      l9.add(new Literal(1, 'y'));
      lm2.add(new Monomio(1, l8));
      Polinomio p2 = new Polinomio(lm2);
      LinkedList<Monomio> lm3 = new LinkedList<Monomio>();
      LinkedList<Literal> l19 = new LinkedList<Literal>();
      l19.add(new Literal(1, 'x'));
      l19.add(new Literal(1, 'y'));
      lm3.add(new Monomio(8, l9));
      Polinomio p3 = new Polinomio(lm3);
      Assert.assertEquals(p2.simplifica(), p3);

      LinkedList<Monomio> lm4 = new LinkedList<Monomio>();
      LinkedList<Literal> l10 = new LinkedList<Literal>();
      LinkedList<Literal> l11 = new LinkedList<Literal>();
      l10.add(new Literal(2, 'x'));
      l10.add(new Literal(1, 'y'));
      lm4.add(new Monomio(2, l10));
      l11.add(new Literal(2, 'x'));
      l11.add(new Literal(1, 'y'));
      lm4.add(new Monomio(-2, l11));
      Polinomio p4 = new Polinomio(lm4);
      LinkedList<Monomio> lm5 = new LinkedList<Monomio>();
      Polinomio p5 = new Polinomio(lm5);
      Assert.assertEquals(p4.simplifica(), p5);

      LinkedList<Monomio> lm6 = new LinkedList<Monomio>();
      LinkedList<Literal> l13 = new LinkedList<Literal>();
      LinkedList<Literal> l14 = new LinkedList<Literal>();
      LinkedList<Literal> l15 = new LinkedList<Literal>();
      l13.add(new Literal(1, 'x'));
      l13.add(new Literal(1, 'y'));
      lm6.add(new Monomio(4, l13));
      l14.add(new Literal(1, 'z'));
      lm6.add(new Monomio(2, l14));
      l15.add(new Literal(1, 'z'));
      lm6.add(new Monomio(-2, l15));
      Polinomio p6 = new Polinomio(lm6);
      LinkedList<Monomio> lm7 = new LinkedList<Monomio>();
      LinkedList<Literal> l16 = new LinkedList<Literal>();
      l16.add(new Literal(1, 'x'));
      l16.add(new Literal(1, 'y'));
      lm7.add(new Monomio(4, l16));
      Polinomio p7 = new Polinomio(lm7);
      Assert.assertEquals(p6.simplifica(), p7);

    }//simplifica

    /**
     * Prueba unitaria para {@link Polinomio#equals}.
     */
    @Test
    public void testEquals(){
      LinkedList<Monomio> lm = new LinkedList<Monomio>();
      LinkedList<Literal> l = new LinkedList<Literal>();
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l.add(new Literal(1, 'x'));
      l.add(new Literal(1, 'y'));
      lm.add(new Monomio(2, l));
      l1.add(new Literal(1, 'z'));
      lm.add(new Monomio(3, l1));
      Polinomio p = new Polinomio(lm);
      LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l2.add(new Literal(1, 'z'));
      lm1.add(new Monomio(3, l2));
      l3.add(new Literal(1, 'x'));
      l3.add(new Literal(1, 'y'));
      lm1.add(new Monomio(2, l3));
      Polinomio p1 = new Polinomio(lm1);
      Assert.assertEquals(p, p1);

      LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l4.add(new Literal(1, 'x'));
      l4.add(new Literal(1, 'y'));
      lm2.add(new Monomio(2, l3));
      Polinomio p2 = new Polinomio(lm2);
      LinkedList<Monomio> lm3 = new LinkedList<Monomio>();
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      LinkedList<Literal> l6 = new LinkedList<Literal>();
      l5.add(new Literal(1, 'x'));
      l5.add(new Literal(1, 'y'));
      lm3.add(new Monomio(2, l5));
      lm3.add(new Monomio(1, l6));
      Polinomio p3 = new Polinomio(lm3);
      Assert.assertNotEquals(p2, p3);

      LinkedList<Monomio> lm4 = new LinkedList<Monomio>();
      LinkedList<Literal> l7 = new LinkedList<Literal>();
      l7.add(new Literal(1, 'x'));
      l7.add(new Literal(1, 'y'));
      lm4.add(new Monomio(2, l7));
      Polinomio p4 = new Polinomio(lm4);
      LinkedList<Monomio> lm5 = new LinkedList<Monomio>();
      LinkedList<Literal> l8 = new LinkedList<Literal>();
      l8.add(new Literal(1, 'x'));
      l8.add(new Literal(1, 'y'));
      lm5.add(new Monomio(4, l3));
      Polinomio p5 = new Polinomio(lm5);
      Assert.assertNotEquals(p4, p5);
    }//equals

    /**
     * Prueba unitaria para {@link Polinomio#suma}.
     */
    @Test
    public void testSuma(){
      LinkedList<Monomio> lm = new LinkedList<Monomio>();
      LinkedList<Literal> l = new LinkedList<Literal>();
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l.add(new Literal(1, 'x'));
      l.add(new Literal(1, 'y'));
      lm.add(new Monomio(2, l));
      l1.add(new Literal(1, 'z'));
      lm.add(new Monomio(3, l1));
      Polinomio p = new Polinomio(lm);
      LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      l2.add(new Literal(1, 'x'));
      l2.add(new Literal(1, 'y'));
      lm1.add(new Monomio(4, l2));
      l3.add(new Literal(1, 'z'));
      lm1.add(new Monomio(1, l3));
      Polinomio p1 = new Polinomio(lm1);
      LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      l4.add(new Literal(1, 'x'));
      l4.add(new Literal(1, 'y'));
      lm2.add(new Monomio(6, l4));
      l5.add(new Literal(1, 'z'));
      lm2.add(new Monomio(4, l5));
      Polinomio p2 = new Polinomio(lm2);
      Assert.assertEquals(p.suma(p1), p2);

      LinkedList<Monomio> lm3 = new LinkedList<Monomio>();
      LinkedList<Literal> li = new LinkedList<Literal>();
      LinkedList<Literal> l6 = new LinkedList<Literal>();
      li.add(new Literal(1, 'x'));
      li.add(new Literal(1, 'y'));
      lm3.add(new Monomio(2, li));
      l6.add(new Literal(1, 'k'));
      lm3.add(new Monomio(1, l6));
      Polinomio p3 = new Polinomio(lm3);
      LinkedList<Monomio> lm4 = new LinkedList<Monomio>();
      LinkedList<Literal> l7 = new LinkedList<Literal>();
      LinkedList<Literal> l8 = new LinkedList<Literal>();
      LinkedList<Literal> l9 = new LinkedList<Literal>();
      l7.add(new Literal(1, 'z'));
      lm4.add(new Monomio(1, l7));
      l8.add(new Literal(1, 'x'));
      l8.add(new Literal(1, 'y'));
      lm4.add(new Monomio(2, l8));
      l9.add(new Literal(1, 'w'));
      lm4.add(new Monomio(3, l9));
      Polinomio p4 = new Polinomio(lm4);
      LinkedList<Monomio> lm5 = new LinkedList<Monomio>();
      LinkedList<Literal> l10 = new LinkedList<Literal>();
      LinkedList<Literal> l11 = new LinkedList<Literal>();
      LinkedList<Literal> l12 = new LinkedList<Literal>();
      LinkedList<Literal> l13 = new LinkedList<Literal>();
      l10.add(new Literal(1, 'x'));
      l10.add(new Literal(1, 'y'));
      lm5.add(new Monomio(4, l10));
      l11.add(new Literal(1, 'k'));
      lm5.add(new Monomio(1, l11));
      l12.add(new Literal(1, 'z'));
      lm5.add(new Monomio(1, l12));
      l13.add(new Literal(1, 'w'));
      lm5.add(new Monomio(3, l13));
      Polinomio p5 = new Polinomio(lm5);
      Assert.assertEquals(p3.suma(p4), p5);
    }

    /**
     * Prueba unitaria para {@link Polinomio#multiplica}.
     */
    @Test
    public void testMultiplica(){
      LinkedList<Monomio> lm = new LinkedList<Monomio>();
      LinkedList<Literal> l = new LinkedList<Literal>();
      LinkedList<Literal> l1 = new LinkedList<Literal>();
      l.add(new Literal(2, 'x'));
      lm.add(new Monomio(1, l));
      l1.add(new Literal(1, 'x'));
      l1.add(new Literal(1, 'y'));
      lm.add(new Monomio(3, l1));
      Polinomio p = new Polinomio(lm);
      LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
      LinkedList<Literal> l2 = new LinkedList<Literal>();
      LinkedList<Literal> l3 = new LinkedList<Literal>();
      LinkedList<Literal> l4 = new LinkedList<Literal>();
      l2.add(new Literal(1,'y'));
      lm1.add(new Monomio(5, l2));
      l3.add(new Literal(1, 'x'));
      lm1.add(new Monomio(4, l3));
      l4.add(new Literal(0, 'x'));
      lm1.add(new Monomio(-5, l4));
      Polinomio p1 = new Polinomio(lm1);
      LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
      LinkedList<Literal> l5 = new LinkedList<Literal>();
      LinkedList<Literal> l6 = new LinkedList<Literal>();
      LinkedList<Literal> l7 = new LinkedList<Literal>();
      LinkedList<Literal> l8 = new LinkedList<Literal>();
      LinkedList<Literal> l9 = new LinkedList<Literal>();
      l5.add(new Literal(2, 'x'));
      l5.add(new Literal(1, 'y'));
      lm2.add(new Monomio(17, l5));
      l6.add(new Literal(3, 'x'));
      lm2.add(new Monomio(4, l6));
      l7.add(new Literal(2, 'x'));
      lm2.add(new Monomio(-5, l7));
      l8.add(new Literal(1, 'x'));
      l8.add(new Literal(2, 'y'));
      lm2.add(new Monomio(15, l8));
      l9.add(new Literal(1, 'x'));
      l9.add(new Literal(1, 'y'));
      lm2.add(new Monomio(-15, l9));
      Polinomio p2 = new Polinomio(lm2);
      Assert.assertEquals(p.multiplica(p1), p2);

    }

}//class
