/**
* Interfaz para el Sujeto.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Sujeto{

  /**
  * Metodo para que los soldados se muevan.
  */
  public void mover();

  /**
  * Metodo para que los soldados ataquen.
  */
  public void ataque();

  /**
  * Metodo para que los soldados se reporten.
  */
  public void reporta();
}
