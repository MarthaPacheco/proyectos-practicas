/**
* Clase para el Enemigo.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Enemigo {

  /*Atributo para guardar la vida del enimigo*/
  private int hpE = 50;

  /**
  * Metodo que regresa la vida del enimigo.
  */
  public int getHPE(){
    return hpE;
  }

  /**
  * Metodo para reducir la vida del Enemigo.
  */
  public void quitarVida(int a){
    hpE -= a;
    if (hpE < 0) {
       hpE = 0;
    }
    System.out.println("Vida del Enemigo: " + hpE);
  }

}//class
