/**
* Interface para tipo de llantas.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Llanta {

  /**
  * Metodo para crear la llanta.
  */
  public void creaLlanta();

  /**
  * Metodo que regresa el tipo de llanta.
  */
  public void getTipo();

  /**
  * Metdo para el costo de la llanta.
  * @return costo de la llanta.
  */
  public double getCosto();

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa();

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque();

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad();

}//clas
