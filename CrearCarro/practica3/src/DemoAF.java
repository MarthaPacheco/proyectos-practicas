import java.util.Scanner;

/**
* Clase abstracta.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class DemoAF {

  /**
  * Metodo para los carros default.
  */
  public static Auto creAuto(double dinero){
    Scanner sc = new Scanner(System.in);
    int opcion;
    Auto cobraparlor = new Auto(new Deportivas(), new Deportiva(), new Deportivo(), new Arpon(),	
                       new Bsimple());
    Auto furianegra = new Auto(new Deportivas(), new Casual(), new Turbo(), new Lanzallamas(),
                      new Reforzado());
    Auto autobus = new Auto(new Simple(), new Camion(), new Diesel(), new Metralleta(),
                   new Bsimple());
    System.out.println("¿Que auto predeterminado quiere?\n" +
                       "1.- Cobra Parlor: " + String.format("%.2f", cobraparlor.getPrecio()) + "\n" +
                       "2.- Furia Negra: " + String.format("%.2f", furianegra.getPrecio()) + "\n" +
                       "3.- Autobus Escolar: " + String.format("%.2f", autobus.getPrecio()));
    opcion = sc.nextInt();
    Auto auto = null;
    switch (opcion) {
      case 1:
        auto = cobraparlor;
        break;
      case 2:
        auto = furianegra;
        break;
      case 3:
        auto = autobus;
        break;
      default:
        System.out.println("Opcion invalida");
    }

    if(auto != null && auto.getPrecio() > dinero){
      System.out.println("No puedes comprar este auto.");
      return null;
    }else{
      return auto;
    }

  }//creAuto


  public static void main(String[] args) {
    Scanner sr = new Scanner(System.in);
    double dinero;
    System.out.println("¿Cual es tu presupuesto?");
    dinero = sr.nextDouble();
    System.out.println("¿Quieres un auto predeterminado o hacer el tuyo?\n"+
                       "1.- predeterminado\n" +
                       "2.- hacer el mio");
    int op;
    op = sr.nextInt();
    if (op == 1) {
      Auto a = creAuto(dinero);
      if (a != null)
        a.muestraAuto();
    } else {
      String op1;
      Llantas llanta = (Llantas)FactoryProducer.getFactory("llanta");
      Motores motor  = (Motores)FactoryProducer.getFactory("motor");
      Carrocerias carroceria = (Carrocerias)FactoryProducer.getFactory("carroceria");
      Armas arma = (Armas)FactoryProducer.getFactory("armas");
      Blindajes blindaje = (Blindajes)FactoryProducer.getFactory("blindajes");

      System.out.println("Elige las partes de tu auto\n" +
                         "Elige las llantas\n" +
                            "deportivas: " + "442.65\n" +
                            "off-road: " + "452.15\n" +
                            "simple: " + "113.12\n" +
                            "oruga: " + "600.00\n");
      op1 = sr.next();
      Llanta llantal = llanta.getLLantas(op1);

      System.out.println("Elige el motor\n" +
                         "diesel: " + "700.35\n" +
                         "deportivo: " + "1700.39\n" +
                         "turbo: " + "1845.65\n");
      op1 = sr.next();
      Motor motorl = motor.getMotor(op1);

      System.out.println("Elige la carroceria\n" +
                         "casual: " + "413.65\n" +
                         "camion: " + "1213.65\n" +
                         "deportiva: " + "400.39\n");
      op1 = sr.next();
      Carroceria carrocerial = carroceria.getCarroceria(op1);

      System.out.println("Elige el blindaje\n" +
                         "simple: " + "1213.65\n" +
                         "tanque: " + "1845.65\n" +
                         "reforzado: " + "1213.65\n");
      op1 = sr.next();
      Blindaje blindajel = blindaje.getBlindaje(op1);

      System.out.println("Elige tu arma\n" +
                         "arpon: " + "213.65\n" +
                         "sierra: " + "453.65\n" +
                         "metralleta: " + "73.45\n" +
                         "cañon: " + "1013.34\n");
      op1 = sr.next();
      Arma armal = arma.getArma(op1);
      Auto auto = new Auto(llantal, carrocerial, motorl, armal, blindajel);
      auto.muestraAuto();
      if(auto != null && auto.getPrecio() > dinero){
        System.out.println("No puedes comprar este auto.");
      }
    }//else

  }
}//class
