/**
* Clase abstracta para la clase padre.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class Baguette {

  /*Atributo para el tipo de baguette*/
  String tipoBa = "baguette desconocida";

  /**
  * Metodo que regresa el tipo de bagutte.
  * @return el tipo de pan que elige.
  */
  public String getTipo(){
    return tipoBa;
  }

  /**
  * Metodo que regresa el costo del ingrediente.
  * @return el costo del producto.
  */
  public abstract double costo();

  /**
  * Metodo que calcula el total de la compra.
  * @return total de compra.
  */
  public abstract double getTotal();

  /**
  * Metdo que imprime la compra.
  */
  public abstract void imprimeCompra();

}
