package proyecto;

import java.util.LinkedList;

/**
* Clase para Polinomio.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/
public class Polinomio{

  /*lista para guardar monomios*/
  private LinkedList<Monomio> monomios;

  /**
  * Constructor de la clase.
  * @param monomios lista para guardar monomios.
  */
  public Polinomio(LinkedList<Monomio> monomios){
    this.monomios = monomios;
  }

  /**
  * Metodo que regresa una representacion en cadena del Polinomio.
  * @return cadena del polinomio.
  */
  @Override
  public String toString(){
    if (monomios == null || monomios.isEmpty()) {
      return "0";
    }
    //cadena que regresamos.
    String s = "";
    for (Monomio a: monomios) {
      s += a.toString() + "+ ";
    }
    return s.substring(0, s.length() - 2);
  }

  /**
  * Metodo que agrupa los terminos semejantes del Polinomio.
  * @return polinomio simplificado.
  */
  public Polinomio simplifica(){
    //variable para guardar los monomios simplificados.
    LinkedList<Monomio> l1 = new LinkedList<Monomio>();
    while (!monomios.isEmpty()) {
      //variable para copiar la lista de monomios.
      LinkedList<Monomio> l = new LinkedList<>(monomios);
      //variable para guardar los terminos semejantes.
      Monomio mo = null;
      for (Monomio a : l){
        if (mo == null){
          mo = a;
          monomios.remove(a);
        } else if (a.compatible(mo)){
            mo = mo.suma(a);
            monomios.remove(a);
          }
      }
      if (mo.getCofi() != 0){
        l1.add(mo);
      }
    }
    return new Polinomio(l1);
  }

  /**
  * Metodo para para comparar dos polinomios.
  * @param o polinomio para comparar.
  * @return <code>true</code> si son iguales, <code>false</code> si no.
  */
  @Override
  public boolean equals(Object o){
    if(o == null || !(o instanceof Polinomio)){
      return false;
    }
    //cast a o para poder comparar.
    Polinomio l = (Polinomio) o;

    if (!(l.monomios.size() == monomios.size())) {
      return false;
    }
    for (int i = 0; i < monomios.size(); i++) {
      if (monomios.containsAll(l.monomios)) {
        return true;
      }
      return false;
    }
    return true;
  }

  /**
  * Metodo para para sumar dos polinomios.
  * @param p polinomio para sumar.
  * @return Un nuevo polinomio.
  */
  public Polinomio suma(Polinomio p){
    //variable para una nueva lista de monomios de p y nuestra lista.
    LinkedList<Monomio> l = new LinkedList<Monomio>();
    l.addAll(monomios);
    l.addAll(p.monomios);
    //variable para guardar l.
    Polinomio p1 = new Polinomio(l);
    return p1.simplifica();
  }

  /**
  * Metodo para para sumar dos polinomios.
  * @param o polinomio para a multiplicar.
  * @return Un nuevo polinomio.
  */
  public Polinomio multiplica(Polinomio o){
    //variable para una nueva lista de monomios de p y nuestra lista.
    LinkedList<Monomio> l = new LinkedList<Monomio>();
    Monomio mo = null;
    for (Monomio a : o.monomios) {
      for (Monomio b : monomios ) {
        mo = a.multiplica(b);
        l.add(mo);
      }
    }
    Polinomio p = new Polinomio(l);
    return p.simplifica();
  }

}//class
