import java.util.ArrayList;
import java.util.List;

/**
* Clase para el Peloton.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public abstract class Peloton {

  /*Atributo para guardar al peloton*/
  protected List<Soldado> peloton = new ArrayList<Soldado>();
  /*Atributo para el comandante*/
  protected Comandante comandante;

  /**
  * Metodo que regresa al comandante.
  */
  public Comandante getComandante(){
    return comandante;
  }



}
