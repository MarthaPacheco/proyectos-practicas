/**
* Clase para carroceria casual.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Casual implements Carroceria {

  /*Varibale para guardar el tipo de carroceria*/
  private String tipo;

  /**
  * Constructor de la clase
  */
  public Casual(){
    tipo = "Carroceria casual";
  }

  /**
  * Metodo para crear la carroceria.
  */
  public void creaCarroceria(){
    tipo = "Carroceria casual";
  }

  /**
  * Metodo que regresa el tipo de carroceria.
  */
  public void getTipo(){
    System.out.println(tipo);
  }

  /**
  * Metdo para el costo del carroceria.
  * @return el costo del carroceria.
  */
  public double getCosto(){
    return 413.65;
  }

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa(){
    return 4;
  }

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque(){
    return 2;
  }

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad(){
    return 2;
  }

}//clas
