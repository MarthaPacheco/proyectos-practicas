public class Auto{
	Llanta llanta;
	Carroceria carroceria;
	Motor motor;
	Arma arma;
	Blindaje blindaje;

	/**
	* Constructor de la clase.
	* @param llanta tipo de llanta.
	* @param carroceria tipo de carroceria.
	* @param motor tipo de motor.
	*	@param arma tipo de arma.
	*	@param blindaje tipo de blindaje.
	*/
	public Auto(Llanta llanta, Carroceria carroceria, Motor motor, Arma arma, Blindaje blindaje){
		this.llanta = llanta;
		this.carroceria = carroceria;
		this.motor = motor;
		this.arma = arma;
		this.blindaje = blindaje;
	}
	/**
	*	Metodo que muestra como queda el auto al final.
	*/
	public void muestraAuto(){
		System.out.println("El auto construido es el siguiente: ");
		llanta.getTipo();
		carroceria.getTipo();
		motor.getTipo();
		arma.getTipo();
		blindaje.getTipo();
		System.out.println("velocidad: " + this.getVelocidad() + "/50");
		System.out.println("defensa: " + this.getDefensa() + "/50");
		System.out.println("ataque: " + this.getAtaque() + "/50");
		System.out.println("Precio total: " + this.getPrecio());
	}

	/**
	*	Metod que regresa la velocidad del auto.
	*/
	public int getVelocidad(){
		return llanta.getVelocidad() + carroceria.getVelocidad() + motor.getVelocidad()
						+ arma.getVelocidad() + blindaje.getVelocidad();
	}

	/**
	*	Metod que regresa la defensa del auto.
	*/
	public int getDefensa(){
		return llanta.getDefensa() + carroceria.getDefensa() + motor.getDefensa()
						+ arma.getDefensa() + blindaje.getDefensa();
	}

	/**
	*	Metod que regresa la ataque del auto.
	*/
	public int getAtaque(){
		return llanta.getAtaque() + carroceria.getAtaque() + motor.getAtaque()
						+ arma.getAtaque() + blindaje.getAtaque();
	}

	/**
	*	Metod que regresa el precio del auto.
	*/
	public double getPrecio(){
		return llanta.getCosto() + carroceria.getCosto() + motor.getCosto()
						+ arma.getCosto() + blindaje.getCosto();
	}



}//class
