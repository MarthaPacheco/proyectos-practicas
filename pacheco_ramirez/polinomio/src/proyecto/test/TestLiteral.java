package proyecto.test;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import proyecto.Literal;

/**
 * Clase para pruebas unitarias de la clase {@link Literal}.
 */
public class TestLiteral {

    /** Expiración para que ninguna prueba tarde más de 5 segundos. */
    @Rule public Timeout expiracion = Timeout.seconds(5);

    /**
     * Prueba unitaria para {@link Literal#toString}.
     */
    @Test
    public void testToString(){
      Literal l1 = new Literal(0, 'x');
      Assert.assertTrue(l1.toString().equals(""));
      Literal l2 = new Literal(1, 'y');
      Assert.assertTrue(l2.toString().equals("y"));
      Literal l3 = new Literal(4, 'z');
      Assert.assertTrue(l3.toString().equals("z^4"));
    }

    /**
     * Prueba unitaria para {@link Literal#equals}.
     */
    @Test
    public void testEquals(){
      Literal l1 = new Literal(3, 'x');
      Literal l2 = new Literal(3, 'x');
      Assert.assertEquals(l1, l2);
      Literal l3 = new Literal(1, 'z');
      Literal l4 = new Literal(2, 'z');
      Assert.assertNotEquals(l3, l4);
      Literal l5 = new Literal(0, 'x');
      Literal l6 = new Literal(0, 'z');
      Assert.assertEquals(l5, l6);
    }

}
