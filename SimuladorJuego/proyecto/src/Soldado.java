import java.util.UUID;

/**
* Clase para los soldados.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Soldado implements Sujeto, Observer {

  /*Atributo para definir la estrategia*/
  private Especialidad especialidad;
  /*Atributo para el nombre*/
  private String nombre;
  /*Atributo para guardar el ID*/
  private String id;

  /**
  * Constructor de la clase.
  */
  public Soldado(String nombre, Especialidad especialidad){
    this.nombre = nombre;
    id = UUID.randomUUID().toString();
    this.especialidad = especialidad;
  }

  /**
  * Metodo para obtener el ID.
  * @return el ID del soldado.
  */
  public String getID(){
    return id;
  }

  /**
  * Metodo para obtener el nombre.
  * @return el nombre del soldado.
  */
  public String getNombre(){
    return nombre;
  }

  /**
  * Metodo para que los soldados se muevan.
  */
  public void mover(){
    System.out.println(nombre + " ");
    especialidad.moverse();
  }

  /**
  * Metodo para que los soldados ataquen.
  * @param enemigo para reducir el hp del enemigo.
  */
  public void ataque(){
    if (Juego.enemigo.getHPE() == 0) {
      return;
    }
    System.out.println(nombre + " ");
    especialidad.atacar(Juego.enemigo);
  }

  /**
  * Metodo para que los soldados se reporten.
  */
  public void reporta(){
    especialidad.reportar(id, nombre);
  }

  /**
  * Metodo para que los soldados se reporten.
  */
  public void update(String estrategia){
    String est = estrategia.toLowerCase();
    switch (est) {
      case "atacar":
        ataque();
        break;
      case "moverse":
        mover();
        break;
      case "reportar":
        reporta();
        break;
      default:
        System.err.println("ERROR");
    }
  }



}//class
