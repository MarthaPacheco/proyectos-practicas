/**
* Interface para tipo de blindaje.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public interface Blindaje {

  /**
  * Metodo para crear el blindaje.
  */
  public void creaBlindaje();

  /**
  * Metodo que regresa el tipo de blindaje.
  */
  public void getTipo();

  /**
  * Metdo para el costo del blindaje.
  * @return costo del blindaje.
  */
  public double getCosto();

  /**
  * Metodo que regresa el nivel de defensa.
  * @return nivel de defensa.
  */
  public int getDefensa();

  /**
  * Metodo que regresa el nivel de ataque.
  * @return nivel de ataque.
  */
  public int getAtaque();

  /**
  * Metodo que regresa el nivel de velocidad.
  * @return nivel de velocidad.
  */
  public int getVelocidad();


}//clas
