/**
* Clase para la fabrica de carrocerias.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Carrocerias extends AbstractFactory {

  /**
  * Metodo que regresa el tipo de componente.
  */
  @Override
	public Object getComponente(String tipoComponente){
		return getCarroceria(tipoComponente);
	}

  /**
  * Metodo que regresa el tipo de carrocerias.
  */
  public Carroceria getCarroceria(String tipoCarroceria){
    if(tipoCarroceria == null){
      return null;
    }
    String car = tipoCarroceria.toLowerCase();
    switch (car) {
      case "casual":
        return new Casual();
      case "camion":
        return new Camion();
      case "deportiva":
        return new Deportiva();
      default:
        return null;
    }
  }
}//class
