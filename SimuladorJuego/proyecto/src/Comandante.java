import java.util.ArrayList;
import java.util.List;

/**
* Clase para los comandantes.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Comandante extends Soldado {

  /*Atributo para gaurdar a los soldados*/
  private List<Soldado> sol;
  /*Atributo para el nombre*/
  private String nombre;

  /**
  * Constructor de la clase.
  */
  public Comandante(String nombre, Especialidad especialidad){
    super(nombre, especialidad);
  }

  /**
  * Metodo que regresa la lista de soldados.
  */
  public List<Soldado> getSoldados(){
		return sol;
	}

  /**
  * Metodo que modifica la lista.
  */
  public void setSoldados(List<Soldado> sola){
		sol = sola;
	}

  /**
  * Metodo para dar la instruccion de moverse.
  */
  public void instruirMov(){
    System.out.println("Avancen");
    for(Soldado s : sol){
			s.update("moverse");
		}
  }

  /**
  * Metodo para dar la instruccion de atacar.
  */
  public void instruirAtaque(){
    System.out.println("Ataquen");
    for(Soldado s : sol){
			s.update("atacar");
		}
  }

  /**
  * Metodo para dar la instruccion de reportarse.
  */
  public void instruirReport(){
    System.out.println("Reportense");
    for(Soldado s : sol){
			s.update("reportar");
		}
  }
  
  @Override
  public void reporta(){
    System.out.println("Comandante");
  }

}//class
