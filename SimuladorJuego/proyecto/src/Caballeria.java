import java.util.UUID;

/**
* Clase para los soldados de caballeria.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/

public class Caballeria implements Especialidad {

  /*Atributo para guardar el HP*/
  private int hp = 20;
  /*Atributo para guardar la distancia*/
  private int distancia = 10;

  /**
  * Metodo para obtener el HP.
  * @return el HP del soldado.
  */
  public int getHP(){
    return hp;
  }

  /**
  * Metodo para obtener el HP.
  * @return el HP del soldado.
  */
  public int getDistancia(){
    return distancia;
  }

  /**
  * Metodo para que los soldados se muevan.
  */
  public void moverse(){
    if (distancia != 0) {
      distancia -= 2;
      if (distancia < 0) {
        distancia = 0;
      }
      System.out.println("Se movio dos unidades " + "distancia: " +  distancia);
    } else {
      System.out.println("Ya estoy al lado del enemigo");
    }
  }

  /**
  * Metodo para que los soldados ataquen.
  * @param enemigo para reducir el hp del enemigo.
  */
  public void atacar(Enemigo enemigo){
    if (distancia == 0 && hp != 0 && enemigo.getHPE() != 0) {
      System.out.println("Atacando");
      enemigo.quitarVida(5);//dismunuir vida del enemigo
    } else if (hp == 0) {
      System.out.println("Estoy muert@");
    } else if (distancia != 0){
      System.out.println("Aun estoy muy lejos");
    }

  }

  /**
  * Metodo para que los soldados se reporten.
  * @param id identificador del soldado.
  * @param nombre nombre del soldado.
  */
  public void reportar(String id, String nombre){
    System.out.println("Caballero: " + nombre + "\n" + "Identificador: " + id + "\n" +
                       "Distancia: " + distancia + "\n" + "Vida restante: " + hp);
  }


}//class
