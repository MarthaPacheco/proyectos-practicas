package proyecto;

import java.util.LinkedList;

/**
* Clase para Polinomio.
* @author Martha Y. Pacheco Ramirez.
* @version 1.0.
*/
public class Proyecto2 {

  public static void main(String[] args) {
    LinkedList<Monomio> lm = new LinkedList<Monomio>();
    LinkedList<Literal> l = new LinkedList<Literal>();
    LinkedList<Literal> l1 = new LinkedList<Literal>();
    l.add(new Literal(1, 'x'));
    l.add(new Literal(3, 'y'));
    lm.add(new Monomio(8, l));
    l1.add(new Literal(7, 'z'));
    lm.add(new Monomio(3, l1));
    Polinomio p = new Polinomio(lm);
    LinkedList<Monomio> lm1 = new LinkedList<Monomio>();
    LinkedList<Literal> l2 = new LinkedList<Literal>();
    LinkedList<Literal> l3 = new LinkedList<Literal>();
    l2.add(new Literal(1, 'x'));
    l2.add(new Literal(5, 'y'));
    lm1.add(new Monomio(4, l2));
    l3.add(new Literal(7, 'z'));
    lm1.add(new Monomio(2, l3));
    Polinomio p1 = new Polinomio(lm1);
    System.out.println("La suma de " + p + "más " + p1 + " es: " + p.suma(p1));
    System.out.println("----------------------------------------------------");

    LinkedList<Monomio> lm3 = new LinkedList<Monomio>();
    LinkedList<Literal> li = new LinkedList<Literal>();
    LinkedList<Literal> l6 = new LinkedList<Literal>();
    li.add(new Literal(3, 'x'));
    li.add(new Literal(3, 'y'));
    lm3.add(new Monomio(9, li));
    l6.add(new Literal(5, 'k'));
    lm3.add(new Monomio(6, l6));
    Polinomio p3 = new Polinomio(lm3);
    LinkedList<Monomio> lm4 = new LinkedList<Monomio>();
    LinkedList<Literal> l7 = new LinkedList<Literal>();
    LinkedList<Literal> l8 = new LinkedList<Literal>();
    LinkedList<Literal> l9 = new LinkedList<Literal>();
    l7.add(new Literal(2, 'z'));
    lm4.add(new Monomio(3, l7));
    l8.add(new Literal(3, 'x'));
    l8.add(new Literal(3, 'y'));
    lm4.add(new Monomio(7, l8));
    l9.add(new Literal(2, 'w'));
    lm4.add(new Monomio(3, l9));
    Polinomio p4 = new Polinomio(lm4);
    System.out.println("La suma de " + p3 + "más " + p4 + " es: " + p3.suma(p4));
    System.out.println("------------------------------------------------------");

    LinkedList<Monomio> lm5 = new LinkedList<Monomio>();
    LinkedList<Literal> l10 = new LinkedList<Literal>();
    LinkedList<Literal> l11 = new LinkedList<Literal>();
    LinkedList<Literal> l12 = new LinkedList<Literal>();
    LinkedList<Literal> l13 = new LinkedList<Literal>();
    l10.add(new Literal(3, 'x'));
    l10.add(new Literal(1, 'y'));
    lm5.add(new Monomio(6, l10));
    l11.add(new Literal(2, 'k'));
    lm5.add(new Monomio(2, l11));
    l12.add(new Literal(6, 'z'));
    lm5.add(new Monomio(7, l12));
    l13.add(new Literal(3, 'w'));
    lm5.add(new Monomio(7, l13));
    Polinomio p5 = new Polinomio(lm5);
    LinkedList<Monomio> lm2 = new LinkedList<Monomio>();
    LinkedList<Literal> l4 = new LinkedList<Literal>();
    LinkedList<Literal> l5 = new LinkedList<Literal>();
    l4.add(new Literal(2, 'x'));
    l4.add(new Literal(3, 'y'));
    lm2.add(new Monomio(6, l4));
    l5.add(new Literal(6, 'z'));
    lm2.add(new Monomio(2, l5));
    Polinomio p2 = new Polinomio(lm2);
    System.out.println("La multiplicación de " + p5 + "por "
    + p2 + " es: " + p5.multiplica(p2));
    System.out.println("----------------------------------------------------");

    LinkedList<Monomio> lm6 = new LinkedList<Monomio>();
    LinkedList<Literal> la = new LinkedList<Literal>();
    LinkedList<Literal> lb = new LinkedList<Literal>();
    la.add(new Literal(2, 'x'));
    lm6.add(new Monomio(4, la));
    lb.add(new Literal(5, 'x'));
    lb.add(new Literal(2, 'y'));
    lm6.add(new Monomio(3, lb));
    Polinomio p6 = new Polinomio(lm6);
    LinkedList<Monomio> lm7 = new LinkedList<Monomio>();
    LinkedList<Literal> lc = new LinkedList<Literal>();
    LinkedList<Literal> ld = new LinkedList<Literal>();
    LinkedList<Literal> le = new LinkedList<Literal>();
    lc.add(new Literal(1,'y'));
    lm7.add(new Monomio(5, lc));
    ld.add(new Literal(1, 'x'));
    lm7.add(new Monomio(4, ld));
    le.add(new Literal(6, 'x'));
    lm7.add(new Monomio(5, le));
    Polinomio p7 = new Polinomio(lm7);
    System.out.println("La multiplicación de " + p6 + "por "
    + p7 + " es: " + p6.multiplica(p7));
    System.out.println("----------------------------------------------------");

    LinkedList<Monomio> lm8 = new LinkedList<Monomio>();
    LinkedList<Literal> lf = new LinkedList<Literal>();
    LinkedList<Literal> lg = new LinkedList<Literal>();
    LinkedList<Literal> lh = new LinkedList<Literal>();
    LinkedList<Literal> lk = new LinkedList<Literal>();
    lf.add(new Literal(2, 'x'));
    lf.add(new Literal(3, 'y'));
    lm8.add(new Monomio(5, lf));
    lg.add(new Literal(3, 'y'));
    lg.add(new Literal(2, 'x'));
    lm8.add(new Monomio(7, lg));
    lh.add(new Literal(3, 'w'));
    lm8.add(new Monomio(9, lh));
    lk.add(new Literal(2, 'y'));
    lm8.add(new Monomio(4, lk));
    Polinomio p8 = new Polinomio(lm8);
    System.out.println("El polinomio " + p8 +  " simplificado es: " + p8.simplifica());


  }//main


}
